# DataTablePager #

Java server-side pagination utility for DataTables jQuery plug-in.

### Usage ###

* Download the jar file in download section and import it into your java web project.
* You also need to download jQuery DataTables plugin http://datatables.net/

### Contribution ###

* If you want to contribute to this project, please contact me.

### Notes ###

* Version 0.3.0 and previous are tested for DataTables.js v 1.9.x

### For suggestions and info ###

* e-mail: **castellettid (at) gmail [dot] com**

### Support project ###

* If you find this project useful, please make a donation: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=265EAR835B5WG