package it.davioooh.datatablepager;

public class PagerException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 634187705070569048L;

	public PagerException() {
		super();
	}

	public PagerException(String message, Throwable cause) {
		super(message, cause);
	}

	public PagerException(String message) {
		super(message);
	}

	public PagerException(Throwable cause) {
		super(cause);
	}

}
