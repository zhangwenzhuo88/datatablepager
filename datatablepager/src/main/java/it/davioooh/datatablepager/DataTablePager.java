package it.davioooh.datatablepager;

public interface DataTablePager {
	DataTableResponse process(DataTableRequest aoData) throws PagerException;
}
