package it.davioooh.datatablepager;

import java.lang.reflect.Field;

import com.google.common.base.Predicate;
import com.google.common.collect.ComparisonChain;

public class TableColumn implements Comparable<TableColumn> {
	public static final String SORT_DIRECTION_ASC = "asc";
	public static final String SORT_DIRECTION_DESC = "desc";

	public static final Predicate<TableColumn> IS_CURRENTLY_SORTED = new Predicate<TableColumn>() {
		@Override
		public boolean apply(TableColumn dtc) {
			boolean sorted = false;
			if (dtc != null) {
				sorted = dtc.isCurrentlySorted();
			}
			return sorted;
		}
	};
	public static final Predicate<TableColumn> IS_SEARCHABLE = new Predicate<TableColumn>() {
		@Override
		public boolean apply(TableColumn dtc) {
			boolean searchable = false;
			if (dtc != null) {
				searchable = dtc.isSearchable();
			}
			return searchable;
		}
	};

	private String name;
	private int columnIndex;
	private boolean searchable;
	private boolean sortable;
	private Field property;
	private int sortOrder;
	private boolean currentlySorted;
	private String sortDirection;

	public TableColumn(int columnIndex, String name) {
		this.name = name;
		this.columnIndex = columnIndex;
	}

	public String getName() {
		return name;
	}

	public int getColumnIndex() {
		return columnIndex;
	}

	public boolean isSearchable() {
		return searchable;
	}

	public void setSearchable(boolean searchable) {
		this.searchable = searchable;
	}

	public boolean isSortable() {
		return sortable;
	}

	public void setSortable(boolean sortable) {
		this.sortable = sortable;
	}

	public int getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}

	public boolean isCurrentlySorted() {
		return currentlySorted;
	}

	public void setCurrentlySorted(boolean currentlySorted) {
		this.currentlySorted = currentlySorted;
	}

	public String getSortDirection() {
		return sortDirection;
	}

	public void setSortDirection(String sortDirection) {
		this.sortDirection = sortDirection;
	}

	public Field getProperty() {
		return property;
	}

	public void setProperty(Field property) {
		this.property = property;
	}

	@Override
	public int compareTo(TableColumn o) {
		return ComparisonChain.start()
				.compare(this.getSortOrder(), o.getSortOrder()).result();
	}

}
