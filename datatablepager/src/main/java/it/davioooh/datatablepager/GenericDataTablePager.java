package it.davioooh.datatablepager;

import it.davioooh.datatablepager.data.PagerRepository;
import it.davioooh.datatablepager.data.PagerRepositoryException;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Strings;
import com.google.common.reflect.TypeToken;

public class GenericDataTablePager<T> implements DataTablePager {

	private PagerRepository<T> repository;
	private TypeToken<T> recordType;

	public GenericDataTablePager(PagerRepository<T> repo) {
		this.repository = repo;
		//
		this.recordType = new TypeToken<T>(getClass()) {
			private static final long serialVersionUID = 1L;
		};
	}

	@SuppressWarnings("unchecked")
	protected Class<T> getRecordType() {
		return (Class<T>) recordType.getRawType();
	}

	public PagerRepository<T> getRepository() {
		return repository;
	}

	@Override
	public DataTableResponse process(DataTableRequest aoData)
			throws PagerException {
		List<TableColumn> columns = parseColumns(aoData);
		return prepResponse(aoData, columns);
	}

	//

	protected List<TableColumn> parseColumns(DataTableRequest aoData)
			throws PagerException {

		List<TableColumn> columns = new ArrayList<TableColumn>();
		if (aoData == null) {
			throw new IllegalArgumentException(
					"aoData can not be null or empty");
		}

		// What column is searchable and / or sortable
		// What properties from T is identified by the columns
		// Field[] properties = this.type.getFields();
		int i = 0;

		// Search and store all properties from T
		for (String col : aoData.getColumns().split(",")) {
			if (!Strings.isNullOrEmpty(col)) {
				TableColumn column = new TableColumn(i, col);
				column.setSearchable(aoData.getSearchColumns().get(i));
				try {
					column.setProperty(getDeclaredField(col));
				} catch (Exception e) {
					throw new PagerException("prepColumns", e);
				}
				columns.add(column);
			}
			i++;
		}

		// Sort
		for (TableColumn column : columns) {
			column.setSortable(aoData.getSortableColumns().get(
					column.getColumnIndex()));
			column.setSortOrder(-1);

			// Is this item amongst currently sorted columns?
			int order = 0;
			for (int sorted : aoData.getSortedColumns()) {
				if (column.getColumnIndex() == sorted) {
					column.setCurrentlySorted(true);

					// Is this the primary sort column or secondary?
					column.setSortOrder(order);

					// Ascending or Descending?
					column.setSortDirection(aoData.getSortDirections().get(
							order));
				}
				order++;
			}
		}

		return columns;
	}

	protected DataTableResponse prepResponse(DataTableRequest aoData,
			List<TableColumn> columns) throws PagerRepositoryException {
		DataTableResponse dtResp = new DataTableResponse();

		// What are the columns in the data set
		dtResp.setColumns(aoData.getColumns());

		// Return same sEcho that was posted. Prevents XSS attacks.
		dtResp.setEcho(aoData.getEcho());

		// REPO SETUP
		this.repository.setColumns(columns);
		this.repository.setSearchValue(aoData.getSearchQuery());

		// Return count of all records
		dtResp.setTotalRecords(this.repository.getTotalEntryCount());

		// Filtered Data

		// What is filtered data set count now. This is NOT the
		// count of what is returned to client
		dtResp.setTotalDisplayRecords(this.repository.getFilteredEntryCount());

		// Take a page
		List<T> pagedRecords = this.repository.getPageEntries(
				aoData.getDisplayStart(), aoData.getDisplayLength());

		List<List<String>> aaDataArray = convertToArray(columns, pagedRecords);

		dtResp.setData(aaDataArray);

		return dtResp;
	}

	protected List<List<String>> convertToArray(List<TableColumn> cols,
			List<T> data) {
		List<List<String>> aaData = new ArrayList<List<String>>();
		for (T o : data) {
			List<String> recordVals = new ArrayList<String>();
			for (TableColumn col : cols) {
				try {
					col.getProperty().setAccessible(true);
					recordVals.add(col.getProperty().get(o).toString());
				} catch (Exception e) {
					recordVals.add("");
				}
			}
			aaData.add(recordVals);
		}
		return aaData;
	}

	//

	public Field getDeclaredField(String field) throws NoSuchFieldException,
			SecurityException {
		Field fld = getRecordType().getDeclaredField(field);
		if (fld == null) {
			if (getRecordType().getSuperclass() != null) {
				fld = getDeclaredField(field);
			}
		}
		return fld;
	}

	// public static List<Field> getAllFields(List<Field> fields, Class<?> type)
	// {
	// for (Field field : type.getDeclaredFields()) {
	// fields.add(field);
	// }
	// if (type.getSuperclass() != null) {
	// fields = getAllFields(fields, type.getSuperclass());
	// }
	// return fields;
	// }

}
