package it.davioooh.datatablepager.data;

import it.davioooh.datatablepager.TableColumn;

import java.util.List;

public interface PagerRepository<T> {

	long getTotalEntryCount() throws PagerRepositoryException;

	long getFilteredEntryCount() throws PagerRepositoryException;

	List<T> getPageEntries(int pageStart, int pageLenght)
			throws PagerRepositoryException;

	void setColumns(List<TableColumn> dtColumns);

	void setSearchValue(String searchValue);
}
