package it.davioooh.datatablepager.data;

import it.davioooh.datatablepager.PagerException;

public class PagerRepositoryException extends PagerException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3638031614172435791L;

	public PagerRepositoryException(String string, Exception ex) {
		super(string, ex);
	}

	public PagerRepositoryException(Exception ex) {
		super(ex);
	}

}
