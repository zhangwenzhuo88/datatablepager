package it.davioooh.datatablepager;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

//  DataTableResponse<T>
public class DataTableResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7484454235436841766L;

	@JsonProperty(value = "iTotalRecords")
	private long totalRecords;

	@JsonProperty(value = "iTotalDisplayRecords")
	private long totalDisplayRecords;

	@JsonProperty(value = "sEcho")
	private int echo;

	@JsonProperty(value = "sColumns")
	private String columns;

	@JsonProperty(value = "aaData")
	private List<List<String>> data = new ArrayList<List<String>>();

	public long getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(long totalRecords) {
		this.totalRecords = totalRecords;
	}

	public long getTotalDisplayRecords() {
		return totalDisplayRecords;
	}

	public void setTotalDisplayRecords(long totalDisplayRecords) {
		this.totalDisplayRecords = totalDisplayRecords;
	}

	public int getEcho() {
		return echo;
	}

	public void setEcho(int echo) {
		this.echo = echo;
	}

	public String getColumns() {
		return columns;
	}

	public void setColumns(String columns) {
		this.columns = columns;
	}

	public List<List<String>> getData() {
		return data;
	}

	public void setData(List<List<String>> pagedRecords) {
		this.data = pagedRecords;
	}
}